angular.module('Components',[])
.controller('MainCtrl', ['$scope', function($scope){
	$scope.cities = ['Chennai','Bangalore','Delhi'];
	$scope.cities1 = ['Madurai', 'Erode','Mumbai'];
	$scope.selectedCities1 = [];
$scope.selectedList = [];
}])
.directive('multiselect', [function(){
	// Runs during compile
	return {
		// name: '',
		// priority: 1,
		// terminal: true,
		 scope: {
		 	'cities' : '=',
		 	'selectedcities' : '='
		 }, // {} = isolate, true = child, false/undefined = no change
		// controller: function($scope, $element, $attrs, $transclude) {},
		// require: 'ngModel', // Array = multiple requires, ? = optional, ^ = check parent elements
		 restrict: 'E', // E = Element, A = Attribute, C = Class, M = Comment
		 template: `<div>
		 				<button class="btn" ng-click="showOptionsDiv()">{{selectedcities.toString()}}</button>
		 				<div ng-if="switch">
		 					<input type="text" placeholder="search" ng-model = "search"/>
		 					<div ng-repeat="item in cities|filter:search">
		 						<input id="opt_{{item}}" type="checkbox" ng-click="composeName()" ng-model="selected[$index]" />
		 							<label for="opt_{{item}}">{{item}}</label>
		 					</div>
		 					<button class="btn btn-primary" ng-click="selectAll(true)">Select All</button>
		 					<button class="btn btn-secondary" ng-click="selectAll(false)">Deselect All</button>
		 					<button class="btn" ng-click="hideOptionsDiv()">Close</button>
		 				</div>
		 			</div>`,
		// templateUrl: 'components/multi-select.html',
		 replace: true,
		// transclude: true,
		// compile: function(tElement, tAttrs, function transclude(function(scope, cloneLinkingFn){ return function linking(scope, elm, attrs){}})),
		link: function(scope, iElm, iAttrs, controller) {
			//Switch to control div show
			scope.switch = false;
			//Function to compose the button name
			scope.selected = [];
			scope.composeName = function(){
				scope.selectedcities = [];
				console.log(scope);
				angular.forEach(scope.selected,(item,i) => {
					if(item){
						scope.selectedcities.push(scope.cities[i]);
					}
				});
				console.log(scope.selectedcities);
				if(!scope.selectedcities[0]){
					scope.selectedcities.push('Select');
				}
			};
			// Compose the initial name
			scope.composeName();

			// Show options div
			scope.showOptionsDiv = function(){
				scope.switch = true;
					scope.composeName();
			};

			// Hide options div
			scope.hideOptionsDiv = function(){
				scope.switch = false
				scope.composeName();
			};

			// SelectAll function
			//		Argument:
			//			boolean - true - selects all
			//					- false - deselects all
			scope.selectAll = function (a){
				angular.forEach(scope.cities,(item,i) => scope.selected[i] = a);
				scope.composeName();
			};
		}
	};
}]);